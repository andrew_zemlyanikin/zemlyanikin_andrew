#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Wordcount exercise
Google's Python class

The main() below is already defined and complete. It calls print_words()
and print_top() functions which you write.

1. For the --count flag, implement a print_words(filename) function that counts
how often each word appears in the text and prints:
word1 count1
word2 count2
...

Print the above list in order sorted by word (python will sort punctuation to
come before letters -- that's fine). Store all the words as lowercase,
so 'The' and 'the' count as the same word.

2. For the --topcount flag, implement a print_top(filename) which is similar
to print_words() but which prints just the top 20 most common words sorted
so the most common word is first, then the next most common, and so on.

Use str.split() (no arguments) to split on all whitespace.

Workflow: don't build the whole program at once. Get it to an intermediate
milestone and print your data structure and sys.exit(0).
When that's working, try for the next milestone.

Optional: define a helper function to avoid code duplication inside
print_words() and print_top().

"""

import re
from operator import itemgetter
from collections import Counter

# +++your code here+++
# Define print_words(filename) and print_top(filename) functions.
# You could write a helper utility function that reads a file
# and builds and returns a word/count dict for it.
# Then print_words() and print_top() can just call the utility function.

###

# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.

def print_word(filename):
    file = open(filename)
    file_read = file.read()
    partition_words = re.sub('\W', ' ', file_read).lower().split()
    count_words = Counter(partition_words)
    dict_cnt_wrds = dict(count_words)
    print "WORD:".ljust(15), "COUNT:"
    for word, count in dict_cnt_wrds.items():
        print word.ljust(15), "%i" % count
    print_top(dict_cnt_wrds)
    return word, count


def print_top(_dict_cnt_wrds):
    sort_dict_cnt_wrds = sorted(_dict_cnt_wrds.items(), key=itemgetter(1), reverse=True)
    i = 1
    print "TOP WORD:".center(15)
    print "POSITION WORD:".ljust(15), "COUNT:"
    for tpl_wrd in sort_dict_cnt_wrds:
        print str(i) + ".", tpl_wrd[0].ljust(15), "%i" % tpl_wrd[1], "\n"
        i += 1
        if i > 20:
            break

def main():
    filename = 'C:/1.txt'
    print_word(filename)

if __name__ == '__main__':
    main()
